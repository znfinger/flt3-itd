#!/usr/bin/env python
""" Description of the functionality of this script """

import sys, os
import numpy as np
try:
    import trFinderFuncs as trf
except ImportError:
    print "trfinderFuncs tool is not in PYTHONPATH."
    exit()
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from Sequence import revcomp as rc
import matplotlib.pyplot as plt
from sklearn import hmm

epsilon = sys.float_info.epsilon

class EventDetector:
    def __init__( self, querySeq, refSeq, window=10, mismatch=2, indelBand=10, bandThreshold=40):#a sensible bandThreshold depends on window size
        self.querySeq = querySeq
        self.refSeq = refSeq
        self.window, self.mismatch = window, mismatch
        self.windowSquared = self.window * self.window
        # indelband is the band around position in matrix 
        # (i.e. upstream window +/- 2 along the upstream diagonal)
        # bandThreshold = the sum of values to permit an inexact match
        self.indelBand, self.bandThreshold = indelBand, bandThreshold

        #flattened matrices on axis=0, so that they don't have to be recalculated if multiple events are being sought per read
        self.RTFT = None 
        self.FTFT = None 
        self.FQFT = None 
        self.RQFT = None 
        self.FQFQ = None
        self.RQFQ = None

        #normalized outputs, pre-hmm
        self.normDel = None
        self.normDup = None
        self.normInv = None
        self.normDupInv = None
        self.normIns = None
        
    def hmmBlock ( self, X ,normRatio,eventRatio):
        startprob = np.array([0.99, 0.01])
        transmat = np.array([[0.9999999995, 0.0000000005], [0.0005, .9995]])
        
        model = hmm.GaussianHMM (2, 'spherical', startprob, transmat)
#        m_means = np.array([[.5], [-.5]])
        m_means = np.array([[normRatio], [eventRatio]])
        m_covars = np.array([[1],[1]])
        #m_covars = np.tile(np.identity(1), (2, 1, 1))
        model.means_ = m_means
        model.covars_ = m_covars
        revX = np.array([X]).transpose()
#        print revX.shape
        hidden_states = model.predict(revX)
        
#        print " ".join(map(str, X))
#        print "".join(map(str, hidden_states))
        return hidden_states

    def DuplicationPredict ( self , hmmFlag = True):
        """Predict location of duplication in reference domain"""
        """ Spits out QVR,RVR vector pair"""
        # Steps:
        # 1: get query/ref projection
        # a.) forward orientation
        if self.normDup == None:
            if self.FQFT == None:
                self.FQFT = self.buildSeqPairProjection (self.querySeq, self.refSeq)
        # 2: get ref/ref projection
            if self.FTFT == None:
                self.FTFT = self.buildSeqPairProjection (self.refSeq, self.refSeq)
        # 3: get normalized projections
            self.normDup = ((1+self.FQFT) / (1+self.FTFT)) -1  #at this point, a normalized projection has been generated
        
        # 4a: Compare forward_q_norm to reverse_q_norm, identify window in which the signals alternate
        if hmmFlag:                    
            hidden_states = self.hmmBlock (4.0 * self.normDup , 0.0 , 1.0) 
            return hidden_states
        else:
            return [self.FQFT , self.FTFT]

    def buildSeqPairProjection (self, seq1, seq2, imagename=None, projectedAxis=0):
        # Steps:
        # 1: get query/ref projection
        #  a.) get dot plot for query vs. reference forward
        forward = trf.CompareSeqPairRef(seq1, seq2, len(seq1), len(seq2), self.window, self.mismatch, self.bandThreshold, self.indelBand)
        #  b.) get dot plot for query vs. reference reverse complement
        reverse = trf.CompareSeqPairRef(rc(seq1), rc(seq2), len(seq1), len(seq2), self.window, self.mismatch, self.bandThreshold, self.indelBand)[::-1,::-1]
        # 2: merge forward and reverse complement by minimum
        #matrix = MinMapThread(for_1_vs_for_2, rc_1_vs_rc_2)
        # 3.) project onto refernce coordinate system (correct for zero values iwth epsilon)
        if imagename == None and (projectedAxis == 1 or projectedAxis == 0):
            return (np.array(np.minimum(forward, reverse).sum(axis=projectedAxis), dtype='f8')+epsilon) / self.windowSquared  
        if imagename != None:
            print >>sys.stderr, "image being printed"
            cdiv = 600
            rlen = len(seq1)#q
            qlen = len(seq2)#r
            cfact = max(int(rlen/cdiv),1)
            matrix1= trf.compressMatrix(np.minimum(forward, reverse),cfact,qlen,rlen)
            plt.imshow(matrix1,cmap = cm.Blues, alpha=0.5)
            matrix2a = trf.CompareSeqPairRef(rc(seq1), seq2, len(seq1), len(seq2),self.window, self.mismatch, self.bandThreshold, self.indelBand)
            matrix2b = trf.CompareSeqPairRef(seq1, rc(seq2), len(seq1), len(seq2),self.window, self.mismatch, self.bandThreshold, self.indelBand)[::-1,::-1]
            matrix2 = trf.compressMatrix(np.minimum(matrix2a,matrix2b)[::-1],cfact,qlen,rlen)
            matrix2 = np.ma.masked_where(matrix2 < self.window, matrix2)
            plt.imshow(matrix1,cmap = cm.Blues)
            plt.imshow(matrix2,cmap = cm.Reds)
            plt.savefig(imagename+".png")

if __name__ == "__main__":
    app = EventDetector(seq1.upper(), seq2.upper())
    #if app.opts.profile:
    #    import cProfile
    #    cProfile.run( 'app.run()', '%s.profile' % sys.argv[0] )

    #app.DuplicationPredict()   
    pass
    
 

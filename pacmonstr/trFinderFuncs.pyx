# cython: profile=True
# cython: wraparound=False
# cython: boundscheck=False

import numpy as np
cimport numpy as np

ctypedef np.float_t DTYPE_FLOAT_t
ctypedef np.int_t dtype_ti
ctypedef np.uint16_t dtype_tuint

cdef inline int int_min(int a, int b): return a if a <= b else b
cdef inline int int_max(int a, int b): return a if a >= b else b

cdef int InexactKmerMatchChar_C(char* kmerA, char* kmerB, int L, int posA, int posB, int toleratedMismatch):#tolerated mismatch is a count of mismatches
    cdef unsigned int position
    cdef int mismatch = 0
    #for position in range (L):
    for position from 0 <= position < L:
        if kmerA[position+posA] != kmerB[position+posB]:
            mismatch += 1
            if mismatch > toleratedMismatch:
                return 0
    return L - mismatch

# Aseq = y-axis sequence
# Bseq = x-axis sequence
def CompareSeqPairRef (char* Aseq, char* Bseq, int La, int Lb, int window, int TolMismatch, int SeedThresh, int prevWindow):
    cdef np.ndarray[dtype_tuint, ndim=2] DP = np.zeros((La, Lb), dtype='uint16')#the type should be small, but must be at least as large as the windowSize**2.
    cdef unsigned int w = 0
    cdef int mscore_i = 0
    cdef unsigned int apos = 0
    cdef unsigned int bpos = 0
    cdef int laiter = (La-window+1)
    cdef int lbiter = (Lb-window+1)
    cdef int prevSum = 0
    cdef unsigned int i = 0
    cdef unsigned int j = 0
    for apos from 0 <= apos < laiter:
        for bpos from 0 <= bpos < lbiter:
            mscore_i = InexactKmerMatchChar_C(Aseq, Bseq, window, apos, bpos, TolMismatch)
            if mscore_i == window:
                for w from 0 <= w < window:
                    DP[apos+w, bpos+w] += mscore_i
            elif mscore_i > 0:
                for i from 0 <= i < int_min(apos, prevWindow):
                    for j from 0 <= j < int_min(bpos, prevWindow):
                        prevSum += DP[apos - i,bpos - j]
                if prevSum > SeedThresh:
                    for w from 0 <= w < window:
                        DP[apos+w,bpos+w] += mscore_i
                prevSum = 0
    return DP

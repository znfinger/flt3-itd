rcDict = dict(zip('ACGTWSMKRYBDHVNacgtwsmkrybdhvn-','TGCAWSKMYRVHDBNtgcawskmyrvhdbn-'))

def revcomp(seq):
   return ''.join(map(lambda x: rcDict[x],seq[::-1]))

def codons(seq, frame): #breaks a sequence into codons according to the specified frame
   if frame in [1,2,3]:
       return [seq[frame-1:][3*i:3*i+3] for i in range(len(seq)//3)]
   elif frame in [-1,-2,-3]:
       rcseq = revcomp(seq)
       return [rcseq[-frame-1:][3*i:3*i+3] for i in range(len(rcseq)//3)]
   elif frame == 0:#6 frame translation option
       frames = []
       rcseq = revcomp(seq)
       print rcseq
       for fr in [1,2,3]:
           frames.append([seq[fr-1:][3*i:3*i+3] for i in range(len(seq)//3)])
       for fr in [-1,-2,-3]:
           frames.append([rcseq[-fr-1:][3*i:3*i+3] for i in range(len(rcseq)//3)])
       return frames
   else:
       print 'Improper frame specified.'

gencode = {
   'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
   'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
   'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
   'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
   'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
   'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
   'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
   'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
   'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
   'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
   'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
   'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
   'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
   'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
   'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
   'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W'}

def translate(seq , frame):
   if frame in [1,2,3]:
       return ''.join([gencode.get(seq[frame-1:][3*i:3*i+3],'X') for i in range(len(seq)//3)])
   elif frame in [-1,-2,-3]:
       rcseq = revcomp(seq)
       return ''.join([gencode.get(rcseq[-frame-1:][3*i:3*i+3],'X') for i in range(len(rcseq)//3)])
   elif frame == 0:#6 frame translation option
       frames = []
       rcseq = revcomp(seq)
       print rcseq
       for fr in [1,2,3]:
           frames.append(''.join([gencode.get(seq[fr-1:][3*i:3*i+3],'X') for i in range(len(seq)//3)]))
       for fr in [-1,-2,-3]:
           frames.append( ''.join([gencode.get(rcseq[-fr-1:][3*i:3*i+3],'X') for i in range(len(rcseq)//3)]))
       return frames
   else:
       print 'Improper frame specified.'

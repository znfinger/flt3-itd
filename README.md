### Introduction ###

FLT3-ITD finder is a simple classification tool for sequences suspected to have internal tandem duplications. It takes as an argument an .m5 file produced by alignment of CCS PacBio reads to a reference interval using blasr. It then generates dotplots using fuzzy kmer matching and performs a matrix projection onto the reference sequence. After normalization of this signal against a reference sequence self projection vector, the results are fed into a two state HMM, which indicates whether a sequence in the reference has been duplicated in the read being examined. Sequences are written to either a reads_pos.fasta or reads_neg.fasta file based on this classification.

### Setup ###
Essential Dependencies:

* Python/2.7
* Cython/0.21.1
* Numpy/1.8.1
* sklearn/0.16 

Additional dependencies:

* blasr (although not required for running flt3-itd, it is required in order to make the input .m5 file)
* matplotlib (only required if user wants to generate images of dotplots for further scrutiny of structural variation. The option for generating plots is not currently implemented.)

Setup requires:

* Compilation of the pacmonstr/trFinderFuncs.pyx using cython.
* Inclusion of ./flt3-itd/pacmonstr/ in PYTHONPATH.

usage:


```
#!python

python sampleClassifier.py -m5 reads.m5

```


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* matthew (dot) pendleton (at) mssm (dot) edu
#!/usr/bin/env python -o

##
# 
##
# 
##
# 
##
# 
##
import os
import sys
import argparse
from collections import Counter
from pacmonstr.ProjectEventGeneral import EventDetector as ed
from pacmonstr.Sequence import revcomp as rc

parser = argparse.ArgumentParser(description = "A structural variant screener. m5 alignment file of reads to reference comes in, two fastas go out. One fasta contains all the SV+ reads, the other the SV- reads.")

parser.add_argument( "-m5", help="Specify an m5 file with all sequences mapped to a single reference sequence." )
args = parser.parse_args()

#####
# memoization dict
# keyed to (start,stop) coords in ref. holds appropriate projection vectors
FTFTs = {}

eventSet = set()
eventCounter = Counter()
readEvents= {}
negOut = open(os.path.abspath(args.m5).split('/')[-1].replace('.m5','_neg.fasta'),'w')
posOut = open(os.path.abspath(args.m5).split('/')[-1].replace('.m5','_pos.fasta'),'w')

for entry in open(args.m5):
    parsedEntry = dict(zip(['qname','qseqlength','qstart','qend','qstrand','tname','tseqlength','tstart','tend','tstrand','score','nMatch','nMismatch','nIns','nDel','MapQV','seq1','bars','seq2'], entry.split()))
    parsedEntry['seq1'] = parsedEntry['seq1'].replace('-','')
    parsedEntry['seq2'] = parsedEntry['seq2'].replace('-','')
    parsedEntry['qseqlength'] = int( parsedEntry['qseqlength'] )
    parsedEntry['qstart'] = int( parsedEntry['qstart'] )
    parsedEntry['qend'] = int( parsedEntry['qend'] )
    parsedEntry['tseqlength'] = int( parsedEntry['tseqlength'] )
    parsedEntry['tstart'] = int( parsedEntry['tstart'] )
    parsedEntry['tend'] = int( parsedEntry['tend'] )
    parsedEntry['score'] = int( parsedEntry['score'] )
    if parsedEntry['tstrand'] == '-':
        parsedEntry['seq1'] = rc(parsedEntry['seq1'] )
        parsedEntry['seq2'] = rc(parsedEntry['seq2'] )
    projection = ed(parsedEntry['seq1'] , parsedEntry['seq2'])

    #memoization
    if (parsedEntry['tstart'],parsedEntry['tend']) in FTFTs:
        projection.FTFT = FTFTs[(parsedEntry['tstart'],parsedEntry['tend'])]
        print 'memoized'
    HMM = projection.DuplicationPredict()
    if (parsedEntry['tstart'],parsedEntry['tend']) not in FTFTs:
        FTFTs[(parsedEntry['tstart'],parsedEntry['tend'])] = projection.FTFT
    if 1 in HMM:
        posOut.write('>%s\n%s\n' % (parsedEntry['qname'],parsedEntry['seq1']))
        print 'pos'
    else:
        negOut.write('>%s\n%s\n' % (parsedEntry['qname'],parsedEntry['seq1']))
        print 'neg'

        
    
                      



